# Serge Crane
# 109923358
# CSE307 HW2

import sys
from itertools import permutations, product, combinations
import time

start_time = time.time()

# things we will need
position = 0
total_layers = 0
layer_index = -1   # to handle starting from 1 or 0
nodes = {}         # list of dictionary that holds the nodes of each layer [node_name, node_layer]
edges = [[], []]         # an empty dictionary to hold all of the edges
layers = []        # list of all possible permutations of [0] = layer0, [1] = layer1

# returns the values between parenthesis in a line as a str
def get_inner(line):
    return line[line.find("(") + 1:line.find(")")]


def build_layers():
    for i in range(0, total_layers):
        layers.append([])
        lperm = permutations(filter_dict_layer(nodes, i))
        for p in lperm:
            layers[i].append(list(p))


def all_edges():
    minx = 999
    for layer0 in layers[0]:
        for layer1 in layers[1]:
            x0 = calc_x(get_edge_pos(layer0, layer1))
            if x0 >= minx:
                continue
            for layer2 in layers[2]:
                x1 = calc_x(get_edge_pos(layer1, layer2))
                x = x0 + x1
                if x < minx:
                    minx = x
    return minx


def get_edge_pos(layer0, layer1):
    # so layer0 and layer1 are a single permutation
    sorted_edges = []
    for edge in edges:
        for edge_tuple in edge:
            #print(edge_tuple)
            # ('n1', '# n4')
            #print("Checking if " + str(edge_tuple[1]))
            if edge_tuple[1] in layer1:
                # NEED TO GET NODE POSITION
                tup = (layer0.index(edge_tuple[0]), layer1.index(edge_tuple[1]))
                sorted_edges.append(tup)
                #print("This tuplet passed: " + str(tup))

    # print(sorted_edges)
    return sorted_edges


# Given edges as positions (src, dest) in a list in one layer, returns the amount of intersections
def calc_x(edges):
    if len(edges) != 0:
        x = 0  # initial number of crossings
        for tup in edges:
            for tup2 in edges:
                if tup2[0] < tup[0]:
                    if tup2[1] > tup[1]:
                        x += 1
                if tup2[0] > tup[0]:
                    if tup2[1] < tup[1]:
                        x += 1
        return int(x/2)
    else:
        return 0


def get_layer_perms(num):
    return layers[num]


def filter_dict_layer(dict, val):
    return {k: v for k, v in dict.items() if val == v}


###########################################

###             MAIN CODE              ####

###########################################
print("Input file is: " + sys.argv[1])

file = open(sys.argv[1], 'r')
lines = file.readlines()  # array of lines in the text file

# Reading the whole file line by line
for line in lines:
    # print(line.strip())        # prints the current line of the file
    line = line.strip()
    if line != "":
        if "layers" in line:
            total_layers = int(get_inner(line))  # Data is the value between the parenthesis
        if "width" in line:
            layer_index += 1
            position = 0
        if "in_layer" in line:
            data = get_inner(line)
            node_name = data.split(',')[1]
            nodes[node_name] = layer_index          # add [node_name, layer_index] to dictionary
            position += 1
        if "edge" in line:
            nodes2 = get_inner(line).split(',')
            # print("Adding edges from " + nodes[0] + " to " + nodes[1])
            edges[nodes.get(nodes2[0].strip())].append((nodes2[0].strip(), nodes2[1].strip()))

build_layers()  # places all the possible iterations of layers in layers list


print("Min edges: " + str(all_edges()))
print("time elapsed: {:.2f}s".format(time.time() - start_time))
